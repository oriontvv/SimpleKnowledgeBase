package my;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class GoArticleAction implements Action {

    public String perform(HttpServletRequest request, HttpServletResponse response) {
        String number = request.getParameter("number");
        int ArticleId = Integer.parseInt(request.getParameter("id"));
        
        HttpSession session = request.getSession();
        chapter main_chapter = (chapter) session.getAttribute("chapters");

        chapter res_chapter;
        if(number == "%20") res_chapter=main_chapter;
        else res_chapter=main_chapter.Findchapter(main_chapter, number);
        
        Article res_art = res_chapter.getArticle(ArticleId);
        MyResult res = new MyResult();
        res.setName("article");
        res.setRes_article(res_art);

        session.setAttribute("res_chapter", res);
        return "/index.jsp";
    }
}
