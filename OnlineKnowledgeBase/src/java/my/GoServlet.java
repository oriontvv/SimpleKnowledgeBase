package my;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GoServlet extends HttpServlet {

    protected ActionFactory factory = new ActionFactory();

    public GoServlet() {
        super();
    }

    protected String getActionName(HttpServletRequest request) {
        String path = request.getServletPath();
        return path.substring(1, path.lastIndexOf("."));
    }

    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Action action = factory.create(getActionName(request));
        String url = action.perform(request, response);
        if (url != null)
            getServletContext().getRequestDispatcher(url).forward(request, response);
    }
}
