package my;

public class MyResult {
    private String name;
    private chapter res_chapter;
    private Article res_article;

    public MyResult() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public chapter getRes_chapter() {
        return res_chapter;
    }

    public void setRes_chapter(chapter res_chapter) {
        this.res_chapter = res_chapter;
    }

    public Article getRes_article() {
        return res_article;
    }

    public void setRes_article(Article res_article) {
        this.res_article = res_article;
    }
}
