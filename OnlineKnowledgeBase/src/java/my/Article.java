package my;

public class Article {
    private String short_name;
    private String name;
    private String text;
    private int id;
    private String number;

    public Article() {
    }

    public  Article (String short_name, String name, String text) {
        this.short_name = short_name;
        this.name = name;
        this.text = text;
    };

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}