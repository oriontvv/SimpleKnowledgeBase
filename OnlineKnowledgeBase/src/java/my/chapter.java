package my;
import java.util.Vector;

public class chapter {

    private String short_name;
    private String name;
    private String number="";
    private String way=" :: ";
    private Vector<Article> article_list = new Vector<Article>();
    private Vector<chapter> chapter_list = new Vector<chapter>();

    public chapter(String short_name,String name) {
        this.short_name = short_name;
        this.name = name;
    }

    public chapter() {
        this.short_name = null;
        this.name = null;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumber() {
        return this.number;
    }

    public Vector<Article> getArticle_list() {
        return article_list;
    }

    public Vector<chapter> getChapter_list() {
        return chapter_list;
    }

    public void AddArticle(Article art) {
        art.setNumber(this.getNumber());
        art.setId(this.article_list.size());
        this.article_list.add(art);
    }

    public void Addchapter(chapter chap) {
        int j = this.getChapter_list().size() + 1;
        chap.setNumber(this.getNumber() + j + ".");
        chap.setWay(this.getWay() + chap.getName() + " :: ");
        this.getChapter_list().add(chap);
    }

    public chapter getSubchapter(int i) {
        if ((i >= 0) && (i < this.getChapter_list().size()))
            return getChapter_list().get(i);
        else return null;
    }

    public Article getArticle(int i) {

            return article_list.get(i);
    }

    public boolean IsEmptychapter(){
        if(this.getChapter_list().isEmpty()) return true;
        else return false;
    }

    public boolean IsEmptyArticle(){
        if( this.getArticle_list().isEmpty()) return true;
        else return false;
    }

    public void setArticle_list(Vector<Article> article_list) {
        this.article_list = article_list;
    }

    public void setChapter_list(Vector<chapter> chapter_list) {
        this.chapter_list = chapter_list;
    }

    public chapter Findchapter(chapter head,String number) {
        if ((number == "") || (number == ".")) return head;
        else
        {
            String buf1 = number.substring(0,number.indexOf("."));
            String buf2;
            if ( number.indexOf(".") == number.lastIndexOf(".")) buf2="";
            else buf2=  number.substring(number.indexOf(".") + 1);
            return head.Findchapter(head.getSubchapter(Integer.parseInt(buf1)-1),buf2);
        }
    }

    public void setWay(String way) {
        this.way = way;
    }

    public String getWay() {
        return way;
    }

}
