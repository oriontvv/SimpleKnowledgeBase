/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package my;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class GoChapterAction implements Action{
    public String perform(HttpServletRequest request, HttpServletResponse response) {
        String number = request.getParameter("number");

        HttpSession session = request.getSession();
        chapter main_chapter = (chapter) session.getAttribute("chapters");

        chapter res_chapter;
        if (number == "%20") res_chapter = main_chapter;
        else res_chapter =  main_chapter.Findchapter(main_chapter, number);

        MyResult res = new MyResult();
        res.setRes_chapter(res_chapter);
        res.setName("chapter");

        session.setAttribute("res_chapter", res);
        return "/index.jsp";
    }
}
