<%@page import="java.lang.Object"%>
<%@page import="java.util.Vector"%>
<%@page import="my.*" %>
<%@include file="init.jsp" %>



<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Online Knowledge Base</title>
    </head>
    <body>
        <jsp:useBean id="chapters" scope="session" class="my.chapter" />
        <jsp:useBean id="res_chapter" scope="session" class="my.MyResult" />

        <table border="0" align="left" >
        <thead>
        <tr align="left">
            <th width="300">
            <%
            chapter main_chapter = (chapter)session.getAttribute("chapters");
            for(int i=0;i< main_chapter.getChapter_list().size();i++) {
            %>                
                <a href="goChapterAction.perform?number=<%= main_chapter.getSubchapter(i).getNumber() %>">                   
                    <%= main_chapter.getSubchapter(i).getNumber() %>
                    <%= main_chapter.getSubchapter(i).getShort_name() %>                   
                </a>
                <br>
                <%
                if (!main_chapter.getSubchapter(i).IsEmptychapter()) {
                    for(int i1=0;i1< main_chapter.getSubchapter(i).getChapter_list().size();i1++) {
                    %>
                    <%= " . . . " %>
                        <a href="goChapterAction.perform?number=<%= main_chapter.getSubchapter(i).getSubchapter(i1).getNumber() %>">
                            <%= main_chapter.getSubchapter(i).getSubchapter(i1).getNumber() %>
                            <%= main_chapter.getSubchapter(i).getSubchapter(i1).getShort_name() %>
                        </a>
                        <br>
                        <%
                        if (!main_chapter.getSubchapter(i).getSubchapter(i1).IsEmptychapter()) {
                            for(int i2=0;i2< main_chapter.getSubchapter(i).getSubchapter(i1).getChapter_list().size();i2++) {
                            %>
                            <%= " . . . . . . " %>
                            <a href="goChapterAction.perform?number=<%= main_chapter.getSubchapter(i).getSubchapter(i1).getSubchapter(i2).getNumber() %>">
                                <%= main_chapter.getSubchapter(i).getSubchapter(i1).getSubchapter(i2).getNumber() %>
                                <%= main_chapter.getSubchapter(i).getSubchapter(i1).getSubchapter(i2).getShort_name() %>
                            </a>
                            <br>
                            <%
                            }
                        }
                        if (!main_chapter.getSubchapter(i).getSubchapter(i1).IsEmptyArticle()) {
                            for(int j2=0;j2< main_chapter.getSubchapter(i).getSubchapter(i1).getArticle_list().size();j2++) {
                            %>
                            <%= " . . . . . . " %>
                            <a href="goArticleAction.perform?number=<%= main_chapter.getSubchapter(i).getSubchapter(i1).getNumber() %>&id=<%= main_chapter.getSubchapter(i).getSubchapter(i1).getArticle(j2).getId()%>">
                                <%= main_chapter.getSubchapter(i).getSubchapter(i1).getArticle(j2).getShort_name() %>
                            </a>
                            <br>
                            <%
                            }
                        }
                    }
                }
                if (!main_chapter.getSubchapter(i).IsEmptyArticle()) {
                    for(int j1=0;j1< main_chapter.getSubchapter(i).getArticle_list().size();j1++) {
                    %>
                        <%= " . . . " %>
                        <a href="goArticleAction.perform?number=<%= main_chapter.getSubchapter(i).getNumber() %>&id=<%= main_chapter.getSubchapter(i).getArticle( j1 ).getId()%>">
                        <%= main_chapter.getSubchapter(i).getArticle(j1).getShort_name() %>
                        </a>
                        <br>
                    <%
                    }
                }
            }
            for(int j=0;j< main_chapter.getArticle_list().size();j++) {
            %>
            <a href="goArticleAction.perform?number=<%= main_chapter.getArticle(j).getNumber() %>&id=<%= main_chapter.getArticle(j).getId()%>" >
                    <%= main_chapter.getArticle(j).getShort_name() %>
                </a>
                <br>
            <%
            }
            %>
        </th>
        <th valign="top">
            <%
            MyResult res =(MyResult)session.getAttribute("res_chapter");
            chapter res_chap = res.getRes_chapter();
            String res_name = res.getName();
            Article res_art = res.getRes_article();

            if (res_name == "chapter") {
            %>
            
                <%= res_chap.getWay() %>
                <br>
                <br>
                <h3>
                <%
                for(int id=0; id <res_chap.getChapter_list().size();id++) {
                %>
                    <%= "--" %>
                    <a href="goChapterAction.perform?number=<%= res_chap.getSubchapter(id).getNumber() %>">
                    <%= res_chap.getSubchapter(id).getName() %>
                    </a>
                    <br> <br>
                    
                <%
                }
                 for(int id=0; id <res_chap.getArticle_list().size();id++) {
                %>
                    <%= "--" %>
                    <a href="goArticleAction.perform?number=<%= res_chap.getArticle(id).getNumber() %>&id=<%= res_chap.getArticle(id).getId()%>">
                    <i><%= res_chap.getArticle(id).getName() %></i>
                    </a>
                    <br> <br>
                <%
                }
                %>
            </h3>
            <%
            }
            else if (res_name == "article") {
            %>
            <%= main_chapter.Findchapter(main_chapter,res_art.getNumber()).getWay() %>
                <h1> <%= res_art.getName() %> </h1>
                <br>
            <%= res_art.getText() %>
            <%
            }
            %>
        </th>
        </tr>
        </thead>
        <tbody>
        </tbody>
        </table>
    </body>
</html>
