<%@page import="java.util.Vector" %>
<%@page import="my.Article" %>
<%@page import="my.chapter" %>

<%
    chapter head = new chapter("","");
    
    Article newarticle;
    chapter newchapter;


    
    newchapter = new chapter("Object-Oriented Programming Concepts","Object-Oriented Programming Concepts");
    head.Addchapter(newchapter);
   
    newchapter = new chapter("Language Basics","Language Basics");
    head.Addchapter(newchapter);
    
    newchapter = new chapter("Classes and Objects","Classes and Objects");
    head.Addchapter(newchapter);
    
    newchapter = new chapter("Interfaces and Inheritance","Interfaces and Inheritance");
    head.Addchapter(newchapter);
    
    newchapter = new chapter("Numbers and Strings","Numbers and Strings");
    head.Addchapter(newchapter);
    
    newchapter = new chapter("Generics","Generics");
    head.Addchapter(newchapter);
    
    newchapter = new chapter("Packages","Packages");
    head.Addchapter(newchapter);

    chapter chapter1 = head.getSubchapter(0);

    newarticle = new Article("What Is an Object?", "What Is an Object?", "An object is a software bundle of related state and behavior. Software objects are often used to model the real-world objects that you find in everyday life. This lesson explains how state and behavior are represented within an object, introduces the concept of data encapsulation, and explains the benefits of designing your software in this manner....");
    chapter1.AddArticle(newarticle);

    newarticle = new Article("What Is a Class?", "What Is a Class?", "A class is a blueprint or prototype from which objects are created. This section defines a class that models the state and behavior of a real-world object. It intentionally focuses on the basics, showing how even a simple class can cleanly model state and behavior.");
    chapter1.AddArticle(newarticle);

    newarticle = new Article("What Is Inheritance?", "What Is Inheritance?", "Inheritance provides a powerful and natural mechanism for organizing and structuring your software. This section explains how classes inherit state and behavior from their superclasses, and explains how to derive one class from another using the simple syntax provided by the Java programming language....");
    chapter1.AddArticle(newarticle);

    newarticle = new Article("What Is an Interface?", "What Is an Interface?", "An interface is a contract between a class and the outside world. When a class implements an interface, it promises to provide the behavior published by that interface. This section defines a simple interface and explains the necessary changes for any class that implements it....");
    chapter1.AddArticle(newarticle);

    newarticle = new Article("What Is a Package?", "What Is a Package?", "A package is a namespace for organizing classes and interfaces in a logical manner. Placing your code into packages makes large software projects easier to manage. This section explains why this is useful, and introduces you to the Application Programming Interface (API) provided by the Java platform....");
    chapter1.AddArticle(newarticle);

    newarticle = new Article("Questions and Exercises","Questions and Exercises: Object-Oriented Programming Concepts","Questions: <br>Real-world objects contain ___ and ___. <br>A software object's state is stored in ___. <br>A software object's behavior is exposed through ___. <br>Hiding internal data from the outside world, and accessing it only through publicly exposed methods is known as data ___. <br>A blueprint for a software object is called a ___. <br>Common behavior can be defined in a ___ and inherited into a ___ using the ___ keyword. <br>A collection of methods with no implementation is called an ___. <br>A namespace that organizes classes and interfaces by functionality is called a ___.");
    chapter1.AddArticle(newarticle);

    chapter chapter2 = head.getSubchapter(1);
    
    newchapter = new chapter("Variables","Variables");
    chapter2.Addchapter(newchapter);
    
    newchapter = new chapter("Operators","Operators");
    chapter2.Addchapter(newchapter);
       
    newarticle = new Article("Primitive Data Types","Primitive Data Types","As you learned in the previous lesson, an object stores its state in fields. <br>int cadence = 0; <br> int speed = 0; <br> int gear = 1; <br>The What Is an Object? discussion introduced you to fields, but you probably have still a few questions, such as: ...");
    chapter2.getSubchapter(0).AddArticle(newarticle);
    
    newarticle = new Article("Arrays", "Arrays", "An array is a container object that holds a fixed number of values of a single type. The length of an array is established when the array is created. After creation, its length is fixed. You've seen an example of arrays already, in the main method of the 'Hello World!' application. This section discusses arrays in greater detail.....");
    chapter2.getSubchapter(0).AddArticle(newarticle);

    newarticle = new Article("Summary of Variables", "Summary of Variables", "The Java programming language uses both 'fields' and 'variables' as part of its terminology. Instance variables (non-static fields) are unique to each instance of a class. Class variables (static fields) are fields declared with the static modifier; there is exactly one copy of a class variable, regardless of how many times the class has been instantiated. ....");
    chapter2.getSubchapter(0).AddArticle(newarticle);

    newarticle = new Article("Assignment, Arithmetic", "Assignment, Arithmetic, and Unary Operators", "The Simple Assignment Operator One of the most common operators that youll encounter is the simple assignment operator =. You saw this operator in the Bicycle class; it assigns the value on its right to the operand on its left: .....");
    chapter2.getSubchapter(1).AddArticle(newarticle);

    newarticle = new Article("Equality, Relational", "Equality, Relational, and Conditional Operators", "The equality and relational operators determine if one operand is greater than, less than, equal to, or not equal to another operand. The majority of these operators will probably look familiar to you as well. Keep in mind that you must use ==, not =, when testing if two primitive values are equal.....");
    chapter2.getSubchapter(1).AddArticle(newarticle);

    newarticle = new Article("Shift Operators", "Bitwise and Bit Shift Operators", "The Java programming language also provides operators that perform bitwise and bit shift operations on integral types. The operators discussed in this section are less commonly used. Therefore, their coverage is brief; the intent is to simply make you aware that these operators exist.....");
    chapter2.getSubchapter(1).AddArticle(newarticle);

    chapter chapter3 = head.getSubchapter(2);

    newchapter = new chapter("Classes","Classes");
    chapter3.Addchapter(newchapter);

    newchapter = new chapter("Objects","Objects");
    chapter3.Addchapter(newchapter);

    newarticle = new Article("Declaring Classes","Declaring Classes","You've seen classes defined in the following way: <br>class MyClass {<br>   //field, constructor, and method declarations<br>}<br>This is a class declaration. The class body (the area between the braces) contains all the code that provides for the life cycle of the objects created from the class: constructors for initializing new objects, declarations for the fields that provide the state of the class and its objects, and methods to implement the behavior of the class and its objects.....");
    chapter3.getSubchapter(0).AddArticle(newarticle);

    newarticle = new Article("Defining Methods","Defining Methods","Here is an example of a typical method declaration: <br>public double calculateAnswer(double wingSpan, int numberOfEngines, double length, double grossTons) {<br>//do the calculation here<br>}<br>The only required elements of a method declaration are the method's return type, name, a pair of parentheses, (), and a body between braces, {}.....");
    chapter3.getSubchapter(0).AddArticle(newarticle);

    newarticle = new Article("Passing Information","Passing Information to a Method or a Constructor","The declaration for a method or a constructor declares the number and the type of the arguments for that method or constructor. For example, the following is a method that computes the monthly payments for a home loan, based on the amount of the loan, the interest rate, the length of the loan (the number of periods), and the future value of the loan: ....");
    chapter3.getSubchapter(0).AddArticle(newarticle);

    newarticle = new Article("Creating Objects","Creating Objects","As you know, a class provides the blueprint for objects; you create an object from a class. Each of the following statements taken from the CreateObjectDemo program creates an object and assigns it to a variable: <br>Point originOne = new Point(23, 94);<br>Rectangle rectOne = new Rectangle(originOne, 100, 200);<br>Rectangle rectTwo = new Rectangle(50, 100);......");
    chapter3.getSubchapter(1).AddArticle(newarticle);

    newarticle = new Article("Using Objects","Using Objects","Once you've created an object, you probably want to use it for something. You may need to use the value of one of its fields, change one of its fields, or call one of its methods to perform an action.......");
    chapter3.getSubchapter(1).AddArticle(newarticle);

    newarticle = new Article("Questions and Exercises","Questions and Exercises: Classes and Objects","Questions<br>1. Consider the following class: <br>public class IdentifyMyParts {<br>public static int x = 7; <br>public int y = 3;<br>a. What are the class variables?<br>b. What are the instance variables?<br>c. What is the output from the following code:....");
    chapter3.AddArticle(newarticle);

    session.setAttribute("chapters", head);

%>