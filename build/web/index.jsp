<%@page import="org.apache.catalina.Session"%>
<%@page import="java.util.Map"%>
<%@page import="javax.swing.text.html.HTMLDocument.Iterator"%>
<%@page import="java.util.Vector"%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@page import="knowledgeBase.*" %>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Java (programming language)</title>
        <style type="text/css">
            body, table, hr {
                color: black;
                background: silver;
                font-family: Verdana, sans-serif;
                font-size: x-small;
            }
        </style>
    </head>
    <body>
        <jsp:useBean id="root" scope="session" class="knowledgeBase.Chapter" />
        <jsp:useBean id="result" scope="session" class="knowledgeBase.Result" />

        <table border="1" width="100%"  >
            <td colspan="2" align="center" valign="top">
                <h1> <b>Java Programming Language</b></h1>
            </td>

            <tr valign="top" >
                <td width=350 >
                    <!-- site structure -->
                    <h3>
                        <% Loader loader = new Loader();//from the database
                                    root = loader.getRootChapter();
                                    session.setAttribute("root", root);
                                    Vector<String> map = root.getMap();%>

                        <b>CONTENTS:</b>
                        <br><br>

                        <% for (int i = 0; i < map.size(); i++) {
                                        //unpack current string
                                        String cur = map.elementAt(i);
                                        int is_article;
                                        if (cur.startsWith(".")) {
                                            is_article = 1;
                                            cur = cur.substring(1, cur.length());
                                        } else {
                                            is_article = 0;
                                        }
                                        int id = Integer.parseInt(cur.substring(0, cur.indexOf(";")));
                                        cur = cur.substring(cur.indexOf(";") + 1, cur.length());

                                        int sh = Integer.parseInt(cur.substring(0, cur.indexOf(",")));
                                        cur = cur.substring(cur.indexOf(",") + 1, cur.length());
                                        for (int j = 0; j < sh; j++) {
                        %>
                        &ensp;&ensp;
                        <% }%>
<!--                        <a href="LinkAction.perform?id=<%--=id%>&is_article=<%=is_article--%>">-->
                        <a href="LinkAction.perform?id=<%= id%>&is_article=<%=is_article%>">
                            <%= cur%></a>
                        <br />
                        <% }%>
                    </h3>
                </td>
                <td>
                    <!-- article's texts-->
                    <!-- navigation -->
                    <h3>
                        &ensp; &ensp; &ensp;
                        <a href="javascript:history.go(-1)" >Back</a>&nbsp;|&nbsp;
                        <a href="LinkAction.perform?id=0&is_article=0" >Main</a>&nbsp;|&nbsp;
                        <a href="javascript:history.go(1)" >Forward</a>&nbsp;
                    </h3>


                    <% if (result.getIs_article() == 1) {%>
                    <!--article page-->
                    <br> <h2> <%=result.getArticle().getFullName()%></h2> <br> <br>
                    <h1><%= result.getArticle().getShortName()%></h1>
                    <%= result.getArticle().getText()%>

                    <% } else if (result.getIs_article() == 0) {%>
                    <!--chapter page-->
                    <br> <h2> <%=result.getChapter().getFullName()%></h2> <br> <br>
                    <h2> &ensp; SubChapters: </h2><br>
                    <ul type="disc">

                        <%Chapter ch = result.getChapter();
                             for (int i = 0; i < ch.getChapters().size(); i++) {
                        %>
                        <li>
                            <%--= ch.getChapters().elementAt(i).getFullName()--%>
                            <a href="LinkAction.perform?id=<%= ch.getChapters().elementAt(i).getId()%>&is_article=0">
                                <h2><%= ch.getChapters().elementAt(i).getShortName()%></h2>
                            </a> <br>
                        </li>

                        <%}%>
                    </ul>

                    <h2>&ensp; Articles: </h2><br>
                    <ul type="circle">
                        <%
                             for (int i = 0; i < result.getChapter().getArticles().size(); i++) {
                                 Article art = result.getChapter().getArticles().elementAt(i);
                        %>
                        <li>
                            <a href="LinkAction.perform?id=<%= art.getId()%>&is_article=1">
                                <h2><%= art.getShortName()%></h2>
                            </a> </li><br>
                            <%}%>
                    </ul>
                    <%
                     } else {%>



                    <!--first page-->
                    <% }%>
                    <!-- navigation -->
                    <h3>
                        &ensp; &ensp; &ensp;
                        <a href="javascript:history.go(-1)" >Back</a>&nbsp;|&nbsp;
                        <a href="LinkAction.perform?id=0&is_article=0" >Main</a>&nbsp;|&nbsp;
                        <a href="javascript:history.go(1)" >Forward</a>&nbsp;
                    </h3>
                </td>
            </tr>
        </table>

    </body>
</html>
