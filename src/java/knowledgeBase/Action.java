
package knowledgeBase;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public interface Action {
    public String perform(HttpServletRequest request, HttpServletResponse responce);
//    public void writeToResponseStream(HttpServletResponse response, String output);
}
