package knowledgeBase;

import java.util.Iterator;
import java.util.Vector;

public class Chapter {

    private String shortName;
    private String fullName;
    private String number;
    private int id;
    private int parent;
    private Vector<Article> articles = new Vector<Article>();
    private Vector<Chapter> chapters = new Vector<Chapter>();
    private Vector<String> map = new Vector<String>();
    public int count_art = 0;
    public int count_chap = 0;

    /**
     * @return the shortName
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * @param shortName the shortName to set
     */
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return the articles
     */
    public Vector<Article> getArticles() {
        return articles;
    }

    /**
     * @param articles the articles to set
     */
    public void setArticles(Vector<Article> articles) {
        this.setArticles(articles);
    }

    /**
     * @return the chapters
     */
    public Vector<Chapter> getChapters() {
        return chapters;
    }

    /**
     * @param chapters the chapters to set
     */
    public void setChapters(Vector<Chapter> chapters) {
        this.chapters = chapters;
    }

    /**
     * @return the number
     */
    public String getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(String number) {
        this.number = number;
    }

    public void addChapter(Chapter newChapter) {
        int newNumber = chapters.size() + 1;
        newChapter.setNumber(number + newNumber + ".");
        newChapter.setFullName(fullName + newChapter.getShortName() + "/");
        chapters.add(newChapter);
    }

    public void addChapter(Chapter root, Chapter newChapter, int parent_id) {
        if (parent_id == root.getId()) {
            int newNumber = root.getChapters().size() + 1;


            newChapter.setNumber(root.getNumber() + newNumber + ".");
            newChapter.setFullName(root.getFullName() + newChapter.getShortName() + "/");
            root.addChapter(newChapter);
            count_chap++;
            return;
        } else {
            for (int i = 0; i < root.getChapters().size(); i++) {

                addChapter(root.getChapters().elementAt(i), newChapter, parent_id);
            }
        }
    }

    public void addArticle(Chapter root, Article newArticle, int parent_id) {
        if (parent_id == root.getId()) {
            count_art++;
            root.addArticle(newArticle);
        } else {
            for (int i = 0; i < root.getChapters().size(); i++) {
                addArticle(root.getChapters().elementAt(i), newArticle, parent_id);
            }
        }
    }

    public void addArticle(Article newArticle) {
        newArticle.setFullName(fullName + newArticle.getShortName());
        articles.add(newArticle);
    }

    public Chapter getChapter(int id) {
        return getChapter(this, id);
    }

    public Chapter getChapter(Chapter parent, int id) {
        if (parent.getId() == id) {
            return parent;
        }
        for (int i = 0; i < parent.getChapters().size(); i++) {
//            if (parent.getChapters().elementAt(i).getId() == id) {
//                return parent.getChapters().elementAt(i);
//            }
//            for (int j = 0; j < parent.getChapters().elementAt(i).getChapters().size(); j++) {
//                if (parent.getChapters().elementAt(i).getChapters().elementAt(j).getId() == id) {
//                    return parent.getChapters().elementAt(i).getChapters().elementAt(j);
//                }
//            }
            // WTF???? why next line doesn't work??
            Chapter parent_chapter = getChapter(parent.getChapters().elementAt(i), id);
            if (parent_chapter != null) {
                return parent_chapter;
            }
        }
        return null;
    }

    public Article getArticle(int id) {
        return getArticle(this, id);
    }

    public Article getArticle(Chapter parent, int id) {
        for (int i = 0; i < parent.getArticles().size(); i++) {
            if (parent.getArticles().elementAt(i).getId() == id) {
                return parent.getArticles().elementAt(i);
            }
        }
        for (int i = 0; i < parent.getChapters().size(); i++) {
            Chapter ch1 = parent.getChapters().elementAt(i);
            for (int j = 0; j < ch1.getArticles().size(); j++) {
                if (ch1.getArticles().elementAt(j).getId() == id) {
                    return ch1.getArticles().elementAt(j);
                }
            }
            for (int j = 0; j < ch1.getChapters().size(); j++) {
                Chapter ch2 = ch1.getChapters().elementAt(j);
                for (int k = 0; k < ch2.getArticles().size(); k++) {
                    if (ch2.getArticles().elementAt(k).getId() == id) {
                        return ch2.getArticles().elementAt(k);
                    }
                }
            }
            //return getArticle(parent.getChapters().elementAt(i), id);
        }
        return new Article();
    }

    private void constructMap(Chapter chapter, int shift) {

        for (int i = 0; i < chapter.getChapters().size(); i++) {
            Chapter cur = chapter.getChapters().elementAt(i);
            String res = "";
            res += cur.getId() + ";";

            res += shift + ",";//create shifts

            res += cur.getNumber();
            res += " " + cur.getShortName();

            map.add(res);

            constructMap(cur, shift + 1);

            for (int j = 0; j < cur.getArticles().size(); j++) {
                res = ".";//label means, that this's article(not chapter)
                res += cur.getArticles().elementAt(j).getId() + ";";

                res += (shift + 1) + ",";//create shifts

                //res += "id=" + cur.getArticles().elementAt(j).getId();
                res += cur.getArticles().elementAt(j).getShortName();
                map.add(res);
            }
        }
    }

    /**
     * @return the map
     */
    public Vector<String> getMap() {
        map.clear();
        constructMap(this, 0);
        return map;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the parent
     */
    public int getParent() {
        return parent;
    }

    /**
     * @param parent the parent to set
     */
    public void setParent(int parent) {
        this.parent = parent;
    }
}
