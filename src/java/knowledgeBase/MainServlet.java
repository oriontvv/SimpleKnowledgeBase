
package knowledgeBase;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class MainServlet extends HttpServlet {

    //protected ActionFactory factory = new ActionFactory();

    public MainServlet() {
        super();
    }

    protected String getActionName(HttpServletRequest request) {
        String path = request.getServletPath();
        return path.substring(1, path.lastIndexOf("."));
    }

    public void service(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       LinkAction action = new LinkAction();
                
        String url = action.perform(request, response);
        if (url != null) {
            getServletContext().getRequestDispatcher(url).forward(request, response);
        }
    }
}
