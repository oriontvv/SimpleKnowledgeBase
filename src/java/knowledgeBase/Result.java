package knowledgeBase;

public class Result {

    private int is_article;
    private Chapter chapter;
    private Article article;

    public Result(int is_article, Chapter chapter, Article article) {
        this.is_article = is_article;
        this.chapter = chapter;
        this.article = article;
    }

    public Result() {
        this.is_article = -1;
    }

    /**
     * @return the is_article
     */
    public int getIs_article() {
        return is_article;
    }

    /**
     * @param is_article the is_article to set
     */
    public void setIs_article(int is_article) {
        this.is_article = is_article;
    }

    /**
     * @return the chapter
     */
    public Chapter getChapter() {
        return chapter;
    }

    /**
     * @param chapter the chapter to set
     */
    public void setChapter(Chapter chapter) {
        this.chapter = chapter;
    }

    /**
     * @return the article
     */
    public Article getArticle() {
        return article;
    }

    /**
     * @param article the article to set
     */
    public void setArticle(Article article) {
        this.article = article;
    }
}
