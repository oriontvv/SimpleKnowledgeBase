package knowledgeBase;

import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Loader {

    private Connection con;
    private Statement statement;  //statement for perform SQL query to driver
    private String query;
    private ResultSet rs, r;

    public Loader() {
        try {
            initDB();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Loader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Loader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Chapter getRootChapter() throws SQLException {
        Chapter root = new Chapter();
        root.setParent(-1);
        root.setId(0);
        root.setNumber("");
        root.setFullName("/");
        root.setShortName("/");

        createHierarchicalStructure(root);

        return root;
    }

    public int test(String table) throws SQLException {
        query = "SELECT * FROM " + table;
        rs = statement.executeQuery(query);
        int res = 0;
        while (rs.next()) {
            res++;
        }
        return res;
    }

    private void createHierarchicalStructure(Chapter parent) throws SQLException {

        query = "SELECT * FROM chapters ORDER BY id DESC";
        rs = statement.executeQuery(query);
        if (rs.first()) {
            int max = rs.getInt("id"); // max id

            for (int par_id = 0; par_id < max; par_id++) {
                query = "SELECT * FROM chapters WHERE parent = " + par_id + " ORDER BY id";
                rs = statement.executeQuery(query);
                while (rs.next()) {
                    Chapter newChapter = new Chapter();
                    newChapter.setId(rs.getInt("id"));
                    newChapter.setFullName(rs.getString("fullName"));
                    newChapter.setShortName(rs.getString("shortName"));
                    newChapter.setParent(parent.getId());

                    parent.addChapter(parent, newChapter, par_id);
                }
                query = "SELECT * FROM articles WHERE parent = " + par_id;
                rs = statement.executeQuery(query);
                while (rs.next()) {
                    Article newArticle = new Article();
                    newArticle.setId(rs.getInt("id"));
                    newArticle.setFullName(rs.getString("fullName"));
                    newArticle.setShortName(rs.getString("shortName"));
                    newArticle.setText(rs.getString("text"));

                    parent.addArticle(parent, newArticle, par_id);
                }
            }
        }

//        //add chapters
//        query = "SELECT * FROM chapters WHERE parent = " + parent.getId() + " ORDER BY id";
//
//        rs = statement.executeQuery(query);
//        while (rs.next()) {
//            Chapter newChapter = new Chapter();
//            newChapter.setId(rs.getInt("id"));
//            newChapter.setFullName(rs.getString("fullName"));
//            newChapter.setShortName(rs.getString("shortName"));
//            newChapter.setParent(parent.getId());
//
//            parent.addChapter(newChapter);
//
//           // createHierarchicalStructure(newChapter);
//        }
//        rs.close();
//
//        //add articles
//        query = "SELECT * FROM articles WHERE parent = " + parent.getId();
//        rs = statement.executeQuery(query);
//        while (rs.next()) {
//            Article newArticle = new Article();
//            newArticle.setId(rs.getInt("id"));
////            newArticle.setFullName(rs.getString("fullName"));
//            newArticle.setShortName(rs.getString("shortName"));
//            newArticle.setText(rs.getString("text"));
//
//            parent.addArticle(newArticle);
//        }
//        rs.close();
    }

    private void initDB() throws ClassNotFoundException, SQLException {
        try {
            //load driver
            Class.forName("com.mysql.jdbc.Driver");

            con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/articlesdb",
                    "root", "pass");

            statement = con.createStatement();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
