package knowledgeBase;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LinkAction implements Action {

    public String perform(HttpServletRequest request, HttpServletResponse response) {
        int id = Integer.parseInt(request.getParameter("id"));
        int is_article = Integer.parseInt(request.getParameter("is_article"));

        HttpSession session = request.getSession();

        Chapter root = (Chapter) session.getAttribute("root");

        Result result = new Result();

        result.setIs_article(1);
        result.setChapter(root);
        if (is_article == 1) {
            result.setIs_article(1);
            result.setArticle(root.getArticle(id));
        } else if (is_article == 0) {
            result.setIs_article(0);
            result.setChapter(root.getChapter(id));
        }
        session.setAttribute("result", result);

        return "/index.jsp";
    }
}
